/*
	This call gets every vase present in the greenhouse and represents them in a table,
	each row represents a vase.
*/
function load_live_data() {
    $.ajax({ 
        type: 'GET', 
        url: 'http://deti-engsoft-05.ua.pt:8080/api/plants', 
        headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (data) { 
            t_body = document.getElementById("t_body");
            $.each(data, function(index, element) {
                var tr = document.createElement("tr");
                var td = document.createElement("td");
                var span = document.createElement("span");
                var node = document.createTextNode(element.vase);
                span.onclick = function (){load_plant_page(element.vase);};
                span.onmouseover = function () {this.style.cursor = "pointer"};
                span.appendChild(node);
                td.appendChild(span);
                tr.appendChild(td);
                
                td = document.createElement("td");
                node = document.createTextNode(element.profile_id);
                td.appendChild(node);
                tr.appendChild(td);

                td = document.createElement("td");
                node = document.createTextNode(element.temperature.toFixed(2));
                td.appendChild(node);
                tr.appendChild(td);

                td = document.createElement("td");
                node = document.createTextNode(element.humidity.toFixed(2));
                td.appendChild(node);
                tr.appendChild(td);

                td = document.createElement("td");
                node = document.createTextNode(element.luminosity.toFixed(2));
                td.appendChild(node);
                tr.appendChild(td);

                td = document.createElement("td");
                node = document.createTextNode(element.co2.toFixed(2));
                td.appendChild(node);
                tr.appendChild(td);

                td = document.createElement("td");
                node = document.createTextNode(element.soil_ph.toFixed(2));
                td.appendChild(node);
                tr.appendChild(td);

                td = document.createElement("td");
                node = document.createTextNode(element.soil_humidity.toFixed(2));
                td.appendChild(node);
                tr.appendChild(td);

                td = document.createElement("td");
                if(element.light_on){
                    node = document.createElement("i");
                    node.style.color = "#ffdd0e";
                    td.style.fontSize = "30px";
                    node.style.marginLeft = "15px";
                    node.className += "fas fa-lightbulb";
                    node.onclick = function () {toogle_light(this);};
                    node.onmouseover = function () {this.style.cursor = "pointer"};
                }
                else{
                    node = document.createElement("i");
                    node.style.color = "grey";
                    td.style.fontSize = "30px";
                    node.style.marginLeft = "15px";
                    node.className += "fas fa-lightbulb";
                    node.onclick = function () {toogle_light(this);};
                    node.onmouseover = function () {this.style.cursor = "pointer"};
                }
                td.appendChild(node);
                tr.appendChild(td);

                td = document.createElement("td");
                if(element.blower_on){
                    node = document.createElement("i");
                    node.style.color = "#51c9e1";
                    node.style.marginLeft = "10px";
                    td.style.fontSize = "30px";
                    node.onclick = function () {toogle_fan(this);}
                    node.onmouseover = function () {this.style.cursor = "pointer"};
                    node.className += "fas fa-dharmachakra fa-spin";     
                }
                else{
                    node = document.createElement("i");
                    node.style.color = "grey";
                    node.style.marginLeft = "10px";
                    td.style.fontSize = "30px";
                    node.onclick = function () {toogle_fan(this);}
                    node.onmouseover = function () {this.style.cursor = "pointer"};
                    node.className += "fas fa-dharmachakra";
                }
                td.appendChild(node);
                tr.appendChild(td);
                t_body.appendChild(tr);
            });
            $('#live_plants_table').DataTable( {
                "sDom": '<"top"i>rt<"bottom"flp><"clear">',
                "paging":   true,
                "ordering": true,
                "info":     false,
                "order": [[ 1, "desc" ]]
            });
        }
    });
}

// TODO: REST API CALL TO ACTUATORS
function toogle_fan(el) {
    if (el.classList.contains("fa-spin")) {
        $.ajax({ 
            type: 'GET', 
            url: 'http://deti-engsoft-05.ua.pt:8080/api/blower/1/off', 
            headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
            data: { get_param: 'value' },
            dataType: 'json',
            success: function (data) { 
                
            }
        });
        el.classList.remove("fa-spin"); 
        el.style.color = "grey"; 
    } else {
        $.ajax({ 
            type: 'GET', 
            url: 'http://deti-engsoft-05.ua.pt:8080/api/blower/1/on', 
            headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
            data: { get_param: 'value' },
            dataType: 'json',
            success: function (data) { 
                
            }
        });
        el.classList.add("fa-spin");
        el.style.color = "#51c9e1"; 
    }
}

// TODO: REST API CALL TO ACTUATORS
function toogle_light(el) {
    if (el.style.color == "rgb(255, 221, 14)") {
        $.ajax({ 
            type: 'GET', 
            url: 'http://deti-engsoft-05.ua.pt:8080/api/light/1/off', 
            headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
            data: { get_param: 'value' },
            dataType: 'json',
            success: function (data) { 
                
            }
        });
        el.style.color = "grey";
    } else {
        $.ajax({ 
            type: 'GET', 
            url: 'http://deti-engsoft-05.ua.pt:8080/api/light/1/on', 
            headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
            data: { get_param: 'value' },
            dataType: 'json',
            success: function (data) { 
                
            }
        });
        el.style.color = "#ffdd0e";
    }        
}
