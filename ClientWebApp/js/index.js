window.onload = load_general_page

function load_general_page() {
    load_page("general.html", load_general_page_contents, null);
    deactivate_tabs();
}

function load_profiles_page() {
    load_page("profiles.html", load_profiles_page_contents, null);
    deactivate_tabs();
}

function load_notifications_page() {
    load_page("notifications.html", load_notifications_page_contents, null);
    deactivate_tabs();
}

function load_security_page() {
    load_page("security.html", load_security_page_contents, null);
    deactivate_tabs();
}

function load_plant_page(vase_id){
    load_page("plant.html", load_plant_page_contents, vase_id);
    deactivate_tabs();
}
function load_general_page_contents(vase_id) {
    document.getElementById('menu_general').classList.add('active');
    load_live_data();
}

function load_profiles_page_contents() {
    document.getElementById('menu_profiles').classList.add('active');
    setProfiles();
}

function load_notifications_page_contents(vase_id) {
    document.getElementById('menu_notifications').classList.add('active');
    load_notifications_data();
}

function load_security_page_contents(vase_id) {
    document.getElementById('menu_security').classList.add('active');
    load_security_data();
}

function deactivate_tabs() {
    var tab_names = ["menu_general", "menu_notifications", "menu_profiles", "menu_security"];
    for (var i = 0; i < tab_names.length; i++)
        document.getElementById(tab_names[i]).classList.remove('active');
}

function load_page(url, okCallBack, vase_id) {
    var xhr
    xhr = new XMLHttpRequest()
    xhr.onreadystatechange = handleStateChange
    xhr.open("GET", url, true)
    xhr.send()

    function handleStateChange() {
        if (xhr.readyState === 4 && xhr.status == 200) {
            document.getElementById("main_content").innerHTML = xhr.responseText
            okCallBack(vase_id)     
        }
    }
}

function load_plant_page_contents(vase_id) {
    getAllCharts(vase_id);
}
