notification_counter = 1;
notifications = {};

/*
	Periodic function responsible for getting notifications.
*/
setInterval(function() {
    $.ajax({ 
		type: 'GET', 
		url: 'http://deti-engsoft-05.ua.pt:8080/api/notifications/'+notification_counter, 
		headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
		data: { get_param: 'value' },
		dataType: 'json',
		success: function (data) { 
            $.each(data, function(index, element) {
                notifications[notification_counter] = {"vase":element.vase, "timestamp":element.timestamp, "description":element.description};
                notification_counter++;
            })
            if (data.length > 0) {
                $('#1_notifications_table').DataTable().clear().destroy();
                for(var key in notifications) {
                    var value = notifications[key];
                    $('#'+value.vase+'_notifications_table').find('#t_body').append('<tr id="notification_'+key+'" ><td>'+value.description+'</td>\
                        <td>'+value.timestamp+'</td>\
                        ');/*<td style="color: white; width: 50px;">\
                        <button style="float:right; width: 34px; margin-right: 10px;" onclick="remove_notification('+key+')" class="btn btn-danger btn-sm">\
                        <i class="far fa-trash-alt"></i></button></tr>';*/
                }
                $('#1_notifications_table').DataTable( {
                    "sDom": '<"top"i>rt<"bottom"flp><"clear">',
                    "paging":   true,
                    "ordering": true,
                    "info":     false,
                    "order": [[ 1, "desc" ]]
                });
            }
		}
    });
    /*$('#1_notifications_table').find('#t_body').prepend(
        '<tr id="notification_'+notification_counter+'" ><td>Yupii</td>\
                          <td>12345</td>'
    );*/
}, 1000)

function load_notifications_data() {
	$.ajax({ 
        type: 'GET', 
        url: 'http://deti-engsoft-05.ua.pt:8080/api/plants', 
        headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
        data: { get_param: 'value' },
           dataType: 'json',
        success: function (data) { 
            var nav_tab = document.getElementById("nav_tab");
            var tab_content = document.getElementById("tab-content");
            var first_ul = true;
            var first_table = true;
            $.each(data, function(index, element) {
                var li = document.createElement("li");
                if (first_ul) {
                    li.setAttribute("class", "active");
                    first_ul = false;                    
                }
                var a = document.createElement("a");
                a.setAttribute("data-toggle", "tab");
                a.setAttribute("href", "#"+element.vase+"_notifications");
                var node = document.createTextNode("Vase "+element.vase);
                a.appendChild(node);
                li.appendChild(a);
                nav_tab.appendChild(li);
                var table = '<table id="'+element.vase+'_notifications_table" \
                    class="table table-hover table-striped table-bordered" style="margin-bottom: 10px;margin-top: 10px"> \
                    <thead><tr><th>Description</th><th>Timestamp</th></tr></thead><tbody id="t_body">';
                        var div = document.createElement("div");
                        div.setAttribute("id", element.vase+"_notifications"); 
                        if (first_table) {
                            div.setAttribute("class", "tab-pane fade in active");
                            first_table = false;
                        } else
                            div.setAttribute("class", "tab-pane fade");
                        for(var key in notifications){
                            var value = notifications[key];
                            if (value.vase == element.vase) {
                                table += '<tr id="notification_'+key+'" ><td>'+value.description+'</td>\
                                      <td>'+value.timestamp+'</td>\
                                      ';/*<td style="color: white; width: 50px;">\
                                      <button style="float:right; width: 34px; margin-right: 10px;" onclick="remove_notification('+key+')" class="btn btn-danger btn-sm">\
                                      <i class="far fa-trash-alt"></i></button></tr>';*/
                            }
                        }
                        table += '</tbody></table>';
                        div.innerHTML = table;
                        tab_content.appendChild(div);
                        $('#'+element.vase+'_notifications_table').DataTable( {
                            "sDom": '<"top"i>rt<"bottom"flp><"clear">',
                            "paging":   true,
                            "ordering": true,
                            "info":     false,
                            "order": [[ 1, "desc" ]]
                        } );
            })
        }
    });
}

function remove_notification(notification_counter) {
    console.log("DELETE NOTIFICATION WITH COUNTER "+notification_counter);
    delete notifications[notification_counter];
    document.getElementById('notification_'+notification_counter).remove();
}

/*
function load_notifications_data() {
	$.ajax({ 
        type: 'GET', 
        url: 'http://deti-engsoft-05.ua.pt:8080/api/plants', 
        headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
        data: { get_param: 'value' },
           dataType: 'json',
        success: function (data) { 
            var nav_tab = document.getElementById("nav_tab");
            var tab_content = document.getElementById("tab-content");
            var first_ul = true;
            var first_table = true;
            $.each(data, function(index, element) {
                var li = document.createElement("li");
                if (first_ul) {
                    li.setAttribute("class", "active");
                    first_ul = false;                    
                }
                var a = document.createElement("a");
                a.setAttribute("data-toggle", "tab");
                a.setAttribute("href", "#"+element.vase+"_notifications");
                var node = document.createTextNode("Vase "+element.vase);
                a.appendChild(node);
                li.appendChild(a);
                nav_tab.appendChild(li);
                var table = '<table class="table table-hover table-striped" style="margin-top: 10px"><thead><tr><th>Description</th><th>Timestamp</th><th>Options</th></tr></thead><tbody id="t_body">';
                $.ajax({ 
                    type: 'GET', 
                    url: 'http://deti-engsoft-05.ua.pt:8080/api/notifications/'+element.vase, 
                    headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
                    data: { get_param: 'value' },
                       dataType: 'json',
                    success: function (data) { 
                        var div = document.createElement("div");
                        div.setAttribute("id", element.vase+"_notifications"); 
                        if (first_table) {
                            div.setAttribute("class", "tab-pane fade in active");
                            first_table = false;
                        } else
                            div.setAttribute("class", "tab-pane fade");
                        $.each(data, function(index2, notification) {
                            table += '<tr><td>'+notification.description+'</td>\
                                      <td>'+notification.timestamp+'</td>\
                                      <td style="color: white; width: 50px;"><button style="float:right; width: 34px; margin-right: 10px;" onclick="alert()" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button></tr>';
                        })
                        table += '</tbody></table>';
                        div.innerHTML = table;
                        tab_content.appendChild(div);
                    }
                });
            })
        }
    });
}*/
