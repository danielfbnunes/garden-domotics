

var charts = {}
var vase = 0;

var types = ["temperature", "humidity", "luminosity", "co2", "soil_ph", "soil_humidity"];
function getAllCharts(vase_id){
    document.getElementById("title").innerText = "Vase " + vase_id  + " Information";
    types.forEach(function(entry) {
        getChart(entry, vase_id);
    });
    vase = vase_id;
}

function getChart(type, vase_id){
    var ctx = document.getElementById(type).getContext('2d');
    
    var dataset = new Array();
    
    var label = new Array();
    
    var length = 0;
    
    $.ajax({ 
        type: 'GET', 
        url: 'http://deti-engsoft-05.ua.pt:8080/api/movingAverage/' + vase_id + '/'+type, 
        headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (data) {
            data.time.forEach(function(entry){
                label.push(entry.substring(2, entry.length-2));
            });

            length = label.length;
            data = {
                label: type+" moving average",
                fill: false,
                borderColor: "red",
                data : data.values,
                borderWidth: 1,
                pointBackgroundColor :"red" ,
                pointRadius : 0
            }
            dataset.push(data);
            $.ajax({ 
                type: 'GET', 
                url: 'http://deti-engsoft-05.ua.pt:8080/api/values/' + vase_id + '/'+type + '/'+length, 
                headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
                data: { get_param: 'value' },
                dataType: 'json',
                success: function (dados) {
                    data = {
                        label: type+" data",
                        fill: false,
                        borderColor: "blue",
                        data : dados,
                        borderWidth: 2,
                        pointBackgroundColor :"blue" ,
                        pointRadius : 3
                    }
                    dataset.push(data);
                    
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                        data: { 
                            labels: label,
                            xAxisID : "Time",
                            yAxisID : "Value",
                            datasets : dataset
                        }, 
                        options:{}
                
                    });
                    charts[type] = lineChart;
                }
            });
        }
    });
    
}

function updateAllCharts(vase_id){
    types.forEach(function(entry) {
        updateChart(vase_id, entry, charts[entry]);
    });
}

function updateChart(vase_id, type, chart){
    $.ajax({ 
        type: 'GET', 
        url: 'http://deti-engsoft-05.ua.pt:8080/api/movingAverage/' + vase_id + '/'+ type, 
        headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (data) {
            if(data.time.length > chart.data.labels.length){
                updateSize = data.time.length - chart.data.labels.length;
                labelsToAppend = data.time.slice(0, updateSize +1);
                $.ajax({ 
                    type: 'GET', 
                    url: 'http://deti-engsoft-05.ua.pt:8080/api/values/' + vase_id + '/'+type + '/'+updateSize, 
                    headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
                    data: { get_param: 'value' },
                    dataType: 'json',
                    success: function (dados) {
                        addData(chart, labelsToAppend, dados);
                    }
                });
            }
        }
    });
}

function addData(chart, label, data) {
    console.log("Adding Data");
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

setInterval(function(){
    updateAllCharts(vase);
    }, 5000
);
