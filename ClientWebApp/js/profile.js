var x = null;
var alarm_row = null;
var row = null;
var addLine = false;
var current_profile = null;
var edit_id = null;

function headers(){
    var jumb = document.getElementById("plants_live_jumbotron");
    var btn = document.getElementById("new_prof");
    document.getElementById("main_div").innerHTML = "";
    document.getElementById("main_div").appendChild(jumb);
    document.getElementById("main_div").appendChild(btn);
}

function myFunction(element) {
    current_profile = (""+$(element).closest('table').attr('id')).split('_')[1];
    addLine = false;
    x = $(element).parents('tr');
    document.getElementById('select_option').value = $(element).parents('tr').find('td').eq(0).text().toLowerCase().replace(new RegExp(' ', 'g'), '_');
    document.getElementById('select_action').value = $(element).parents('tr').find('td').eq(2).text().toLowerCase().replace(new RegExp(' ', 'g'), '_');
    document.getElementById($(element).parents('tr').find('td').eq(1).text().split(' ')[0]).checked = true;
    document.getElementById("modal_value").value = $(element).parents('tr').find('td').eq(1).text().split(' ')[1];
    $("#myModal").modal();
    edit_id = (""+$(element).attr("id")).split("_")[1];
}

function addAlarm(table_row){
    current_profile = (""+$(table_row).closest('table').attr('id')).split('_')[1];
    document.getElementById('select_option').value = "";
    document.getElementById('select_action').value = "";
    document.getElementById("modal_value").value = "";
    document.getElementById('Above').checked = false;
    document.getElementById('Below').checked = false;
    addLine = true;
    $("#myModal").modal();
    alarm_row = $(table_row).closest('tr');
}

function modalClose(){
    if (addLine){
        var above = null;
        if ($("input[name=abv_bel]").filter(":checked").val() == "Above"){
            above = 1;
        }else{
            above = 0;
        }
        var formURL = "http://deti-engsoft-05.ua.pt:8080/api/definition_insert";
        $.ajax({
            url: formURL,
            type: "POST",
            data: {
                'above': above,
                'action': document.getElementById('select_action').options[document.getElementById('select_action').selectedIndex].text.toLowerCase(),
                'type': document.getElementById('select_option').options[document.getElementById('select_option').selectedIndex].text.toLowerCase(),
                'value':document.getElementById("modal_value").value,
                'profile_id': current_profile
            },
            success: function() {
                headers();
                setProfiles();
            },
            error: function(jqXHR, status, error) {
                console.log(status + ": " + error);
            }
        });
        //$("<tr><td>"+ document.getElementById('select_option').options[document.getElementById('select_option').selectedIndex].text.toLowerCase() +"</td><td>"+$("input[name=abv_bel]").filter(":checked").val()+" "+document.getElementById("modal_value").value+" send notification.</td><td>"+ document.getElementById('select_action').options[document.getElementById('select_action').selectedIndex].text.toLowerCase() +"</td><td style='color: white; width: 100px;'><button style='width: 34px;'  onclick='myFunction(this)'  class='btn btn-success btn-sm'><i class='far fa-edit'></i></button>&nbsp<button style='width: 34px;' class='removeVaseButton btn btn-danger btn-sm'><i class='far fa-trash-alt'></i></button></td></tr>").insertBefore(alarm_row);
    }else{
        var above = null;
        if ($("input[name=abv_bel]").filter(":checked").val() == "Above"){
            above = 1;
        }else{
            above = 0;
        }
        var formURL = "http://deti-engsoft-05.ua.pt:8080/api/definition_update";
        $.ajax({
            url: formURL,
            type: "POST",
            data: {
                'id': edit_id,
                'above': above,
                'action': document.getElementById('select_action').options[document.getElementById('select_action').selectedIndex].text.toLowerCase(),
                'type': document.getElementById('select_option').options[document.getElementById('select_option').selectedIndex].text.toLowerCase(),
                'value':document.getElementById("modal_value").value,
                'profile_id': current_profile
            },
            success: function() {
                headers();
                setProfiles();
            },
            error: function(jqXHR, status, error) {
                console.log(status + ": " + error);
            }
        });
        //x.find('td').eq(0).text(document.getElementById('select_option').options[document.getElementById('select_option').selectedIndex].text.toLowerCase());
        //x.find('td').eq(1).text($("input[name=abv_bel]").filter(":checked").val() + " " + document.getElementById("modal_value").value + " send notification.");
        //x.find('td').eq(2).text(document.getElementById('select_action').options[document.getElementById('select_action').selectedIndex].text.toLowerCase());
    }
}

$(document).on('click', '.removeVaseButton', function (e) {
    //verificacoes
    e.preventDefault(); 
    var formURL = "http://deti-engsoft-05.ua.pt:8080/api/vases_remove";
    $.ajax({
        url: formURL,
        type: "POST",
        data: {
            'id' : (""+$(this).attr("id")).split("_")[1]
        },
        success: function() {
            headers();
            setProfiles();
        },
        error: function(jqXHR, status, error) {
            console.log(status + ": " + error);
        }
    });

    //$(this).closest('tr').remove();

 });

 $(document).on('click', '.removeDefinitionButton', function (e) {
    //verificacoes
    e.preventDefault(); 
    var formURL = "http://deti-engsoft-05.ua.pt:8080/api/definition_remove";
    $.ajax({
        url: formURL,
        type: "POST",
        data: {
            'id' : (""+$(this).attr("id")).split("_")[1]
        },
        success: function() {
            headers();
            setProfiles();
        },
        error: function(jqXHR, status, error) {
            console.log(status + ": " + error);
        }
    });

    //$(this).closest('tr').remove();

 });

$(document).on('click', '.add_vase_btn', function () {
    //verificacoes
    current_profile = (""+$(this).closest('table').attr('id')).split('_')[1];
    row = $(this).closest('tr');
    document.getElementById('vaseInput').value = "";
    document.getElementById('vase_modal').value = current_profile;
    $('#addVase').modal('show');
});

$(document).on('click', '#submitVase', function () {
    $("#vase_form").submit();
});

$(document).on('submit', '#vase_form', function(e) {
    e.preventDefault();
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax({
        url: formURL,
        type: "POST",
        data: postData,
        success: function() {
            $('#addVase').modal('toggle');
            headers();
            setProfiles();
        },
        error: function(jqXHR, status, error) {
            console.log(status + ": " + error);
        }
    });

    //var newRow = "<tr><td>Vase " +  document.getElementById("vaseInput").value + "</td> <td style=\"color: white; width: 75px;text-align: center\"><button style=\"width: 34px;\"  class=\" removeVaseButton btn btn-danger btn-sm\"><i class=\"far fa-trash-alt\"></i></button></td></tr>"
    //$(newRow).insertBefore(document.getElementById("vases_"+current_profile).rows[document.getElementById("vases_"+current_profile).rows.length - 1]);
});


function newProfile(){
    $("#newProfile").modal();
}

$(document).on('click', '#submitProfile', function () {
    $("#profile_form").submit();
});

$(document).on('submit', '#profile_form', function(e) {
    e.preventDefault();
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax({
        url: formURL,
        type: "POST",
        data: postData,
        success: function() {
            $('#newProfile').modal('toggle');
            headers();
            setProfiles();
        },
        error: function(jqXHR, status, error) {
            console.log(status + ": " + error);
        }
    });
});

function setProfiles(){
    var el = document.getElementById("main_div");
    $.ajax({ 
        type: 'GET', 
        url: 'http://deti-engsoft-05.ua.pt:8080/api/profiles',
        headers: {'Access-Control-Allow-Origin': 'http://deti-engsoft-05.ua.pt:8080'}, 
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (data) {
            $.each(data, function(index, element) {
                var div = document.createElement("div");
                div.style.overflow = "auto";
                var h3 = document.createElement("h3");
                var node_text = document.createTextNode("Profile - " + element.title);
                h3.appendChild(node_text);
                div.appendChild(h3);

                var table = document.createElement("table");
                table.className = "table table-bordered table-striped";
                table.style.width = "60%";
                table.style.position = "relative";
                table.style.float = "left";
                table.style.marginBottom = "75px";
                table.id = "profiles_"+element.profile_id;
                
                
                var thead = document.createElement("thead");
                var tr = document.createElement("tr");
                var th = document.createElement("th");
                var node_text = document.createTextNode("Alarm");
                th.appendChild(node_text)
                tr.appendChild(th);
                th = document.createElement("th");
                node_text = document.createTextNode("Description");
                th.appendChild(node_text)
                tr.appendChild(th);
                th = document.createElement("th");
                node_text = document.createTextNode("Action");
                th.appendChild(node_text)
                tr.appendChild(th);
                th = document.createElement("th");
                node_text = document.createTextNode("Options");
                th.appendChild(node_text)
                tr.appendChild(th);
                thead.appendChild(tr);

                table.appendChild(thead);

                var tbody = document.createElement("tbody");
                tbody.id = "t_body";
                $.each(element.definitions, function(index, e) {
                    var tr_desc = document.createElement("tr");
                    var td_desc = document.createElement("td");
                    var node_text = document.createTextNode(e.type);
                    td_desc.appendChild(node_text);
                    tr_desc.appendChild(td_desc);
                    td_desc = document.createElement("td");
                    if (e.above){
                        node_text = document.createTextNode("Above " + e.value + " send notification.");
                    }else{
                        node_text = document.createTextNode("Below " + e.value + " send notification.");
                    }
                    td_desc.appendChild(node_text);
                    tr_desc.appendChild(td_desc);
                    td_desc = document.createElement("td");
                    node_text = document.createTextNode(e.action);
                    td_desc.appendChild(node_text);
                    tr_desc.appendChild(td_desc);
                    var td_btn = document.createElement("td");
                    td_btn.style.color = "white";
                    td_btn.style.width = "100px";
                    var btn_edit = document.createElement("button");
                    btn_edit.style.width = "34px";
                    btn_edit.style.marginRight = "10px";
                    btn_edit.onclick= function () {myFunction(this);};
                    btn_edit.className="btn btn-success btn-sm";
                    btn_edit.id="updateDef_"+e.def_id;
                    var i_edit = document.createElement("i");
                    i_edit.className = "far fa-edit";
                    btn_edit.appendChild(i_edit);
                    td_btn.appendChild(btn_edit);

                    var btn_rmv = document.createElement("button");
                    btn_rmv.className="removeDefinitionButton btn btn-danger btn-sm";
                    btn_rmv.id="removeDef_"+e.def_id;
                    var i_rmv = document.createElement("i");
                    i_rmv.className = "far fa-trash-alt";
                    btn_rmv.appendChild(i_rmv);
                    td_btn.appendChild(btn_rmv);

                    tr_desc.appendChild(td_btn);
                    tbody.appendChild(tr_desc);
                
                });
                
                var tr_alarm = document.createElement("tr");
                var td_alarm = document.createElement("td");
                td_alarm.colSpan = "4";
                td_alarm.style.backgroundColor = "white !important";
                td_alarm.style.width = "100px";
                td_alarm.style.textAlign = "center";
                td_alarm.style.borderBottomColor = "white";
                td_alarm.style.borderLeftColor = "white";
                td_alarm.style.borderRightColor = "white";
                var btn_alarm = document.createElement("button");
                btn_alarm.style.lineHeight="25px";
                btn_alarm.style.marginTop="10px";
                btn_alarm.style.width="30%";
                btn_alarm.onclick = function () {addAlarm(this);};
                btn_alarm.className = "btn btn-success btn-md";
                var node = document.createTextNode("Add Alarm");
                btn_alarm.appendChild(node);
                td_alarm.appendChild(btn_alarm);
                tr_alarm.appendChild(td_alarm);
                tbody.appendChild(tr_alarm);
                table.appendChild(tbody);
                div.appendChild(table);
                

                var table = document.createElement("table");
                table.className = "table table-bordered table-striped";
                table.style.marginLeft="10%";
                table.style.width = "30%";
                table.style.position = "relative";
                table.style.float = "left";
                table.style.marginBottom = "75px";
                table.id = "vases_"+element.profile_id;

                var thead = document.createElement("thead");
                var tr = document.createElement("tr");
                var th = document.createElement("th");
                th.style.width = "150px";
                var node_text = document.createTextNode("Vases with this profile:");
                th.appendChild(node_text)
                tr.appendChild(th);
                th = document.createElement("th");
                node_text = document.createTextNode("Options");
                th.appendChild(node_text)
                tr.appendChild(th);
                thead.appendChild(tr);

                table.appendChild(thead);

                var tbody = document.createElement("tbody");
                $.each(element.vases, function(index, e) {
                    var tr_desc = document.createElement("tr");
                    var td_desc = document.createElement("td");
                    var node_text = document.createTextNode("Vase " + e.vase);
                    td_desc.appendChild(node_text);
                    tr_desc.appendChild(td_desc);
                    td_desc = document.createElement("td");
                    td_desc.style.color = "white";
                    td_desc.style.width = "75p";
                    td_desc.style.textAlign = "center";
                    var btnRemove = document.createElement("button");
                    btnRemove.style.width = "34px";
                    btnRemove.className = "removeVaseButton btn btn-danger btn-sm";
                    btnRemove.id="remove_"+e.id;
                    var i_rmv = document.createElement("i");
                    i_rmv.className = "far fa-trash-alt";
                    btnRemove.appendChild(i_rmv);
                    td_desc.appendChild( btnRemove);
                    tr_desc.appendChild(td_desc);
                    tbody.append(tr_desc);
                });

                var tr_alarm = document.createElement("tr");
                var td_alarm = document.createElement("td");
                td_alarm.colSpan = "2";
                td_alarm.style.backgroundColor = "white !important";
                td_alarm.style.width = "100px";
                td_alarm.style.textAlign = "center";
                td_alarm.style.borderBottomColor = "white";
                td_alarm.style.borderLeftColor = "white";
                td_alarm.style.borderRightColor = "white";
                var btn_alarm = document.createElement("button");
                btn_alarm.style.lineHeight="25px";
                btn_alarm.style.marginTop="10px";
                btn_alarm.style.width="50%";
                btn_alarm.className = "add_vase_btn btn btn-info btn-md";
                var node = document.createTextNode("Add Vase");
                btn_alarm.appendChild(node);
                td_alarm.appendChild(btn_alarm);
                tr_alarm.appendChild(td_alarm);
                tbody.appendChild(tr_alarm);

                table.appendChild(tbody);

                div.appendChild(table);

                el.appendChild(div);
                
            });
        }
    });
}
