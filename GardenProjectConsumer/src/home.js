/*
	This call gets every vase present in the greenhouse and represents them in a table,
	each row represents a vase.
*/
$.ajax({ 
    type: 'GET', 
    url: 'http://localhost:8080/api/plants', 
    headers: {'Access-Control-Allow-Origin': 'http://localhost:8080'}, 
    data: { get_param: 'value' },
       dataType: 'json',
    success: function (data) { 
        t_body = document.getElementById("t_body");
        $.each(data, function(index, element) {
            var tr = document.createElement("tr");
            var td = document.createElement("td");
            var a = document.createElement("a");
            var node = document.createTextNode(element.vase);
            a.appendChild(node);
            a.setAttribute("href", "plant.html#" + element.vase);
            td.appendChild(a);
            tr.appendChild(td);
            
            td = document.createElement("td");
            node = document.createTextNode(element.plant_type);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.temperature);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.humidity);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.light);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.co2);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.soil_ph);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.soil_humidity);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            if(element.light_on){
                node = document.createTextNode("On");
            }
            else{
                node = document.createTextNode("Off");
            }
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            if(element.blower_on){
                node = document.createTextNode("On");
            }
            else{
                node = document.createTextNode("Off");
            }
            td.appendChild(node);
            tr.appendChild(td);

            t_body.appendChild(tr);
        });
    }
});