package gardenproject.consumer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class ApiAccess {
    /**
     * Reference to the Notification Broker.
     */
    @Autowired
    NotificationMap notification_map;
    
    /**
     * Reference to the Kafka Producer.
     */
    @Autowired
    private KafkaProducer kafkaProducer;
    
    /**
     * Reference to the SQL storage with the information about the vases.
     */
    @Autowired
    StatesRepository storage;
    @Autowired
    NotificationsRepository notificationStorage;
    @Autowired
    ProfileRepository profile_storage;
    /**
     * Api call to get all the Vases information present in the greenhouse.
     * @return Returns a Map with the id of the vase as key and a Map with the information as value.
    */
    @GetMapping("/plants")
    public HashMap<Integer, HashMap<String,Object>> getPlants(){   
        HashMap<Integer, HashMap<String,Object> > plant_map = new HashMap<>();
        for(SensorData data : storage.getAllStates()){
            profile_storage.getVaseProfile(data.getVase());
            if(plant_map.containsKey(data.getVase())){
                plant_map.get(data.getVase()).put(data.getType(), data.getValue());
            }   
            else{
                HashMap<String, Object> state_map = new HashMap<>();
                state_map.put(data.getType(), data.getValue());
                state_map.put("vase", (double)data.getVase());
                state_map.put("profile_id", profile_storage.getVaseProfile(data.getVase()).get(0).getTitle());
                plant_map.put(data.getVase(), state_map);
            }
        }
        return plant_map;
    }
    
    /**
     * Api call used to get the information about a specific plant.
     * 
     * @param vase its the id of the vase for which we want to get the historical information.
     * @return 
     */
    @GetMapping("/plants/{vase}")
    public HashMap<Date, HashMap<String, Double>> getPlant(@PathVariable("vase") int vase){
        HashMap<Date, HashMap<String, Double>> plant_map = new HashMap<>();
        for(SensorData data : storage.getHistory(vase)){
            if(plant_map.containsKey(data.getTimestamp())){
                plant_map.get(data.getTimestamp()).put(data.getType(), data.getValue());
            }else{
                HashMap<String, Double> statesMap = new HashMap<>();
                statesMap.put(data.getType(), data.getValue());
                plant_map.put(data.getTimestamp(), statesMap);
            }            
        }
        return plant_map;
    }

    
    
    /**
     * Api call used to turn the light on/off of a specific vase.
     * 
     * @param id ID of the Vase that we want to turn the light on/off
     * @param action Action that we want to do to the light (on/off)
     */
    @GetMapping("/light/{id}/{action}")
    public void turnLight(@PathVariable("id") int id, @PathVariable("action") String action)
    {
        String payload = "{\"id\":" + id + ", \"type\":\"light\", \"action\":\"" + action + "\"}";
        kafkaProducer.send("mqttsink30", payload);
    }
    
    /**
     * Api call used to turn the blower on/off of a specific vase.
     * 
     * @param id ID of the Vase that we want to turn the blower on/off
     * @param action Action that we want to do to the blower (on/off)
     */
    @GetMapping("/blower/{id}/{action}")
    public void turnBlower(@PathVariable("id") int id, @PathVariable("action") String action)
    {
        String payload = "{\"id\":" + id + ", \"type\":\"blower\", \"action\":\"" + action + "\"}";
        kafkaProducer.send("mqttsink30", payload);
    }
    
    /**
     * Api call to get all notifications.
     * 
     * @return Map with vase id as key and a List with the notifications.
     */
    @GetMapping("/notifications/{id}")
    public List<Notification> notifications(@PathVariable("id") int id){
        return notificationStorage.getNotifications(id);
    }
    
    /**
     * Api call to get all door notifications.
     * 
     * @return Map with vase id as key and a List with the notifications.
     */
    @GetMapping("/door-notifications/{id}")
    public List<Notification> doorNotifications(@PathVariable("id") int id){
        return notificationStorage.getDoorNotifications(id);
    }
    
    /**
     * Api call to see if the door is open or close.
     * 
     * @return true if door is open. close otherwise.
     */
    @GetMapping("/door-state")
    public Boolean doorState(){
        List<Notification> last_notification = notificationStorage.isDoorOpen();
        if (last_notification.isEmpty())
            return false;
        String[] desc_arr = last_notification.get(0).getDescription().trim().split(" ");
        if (desc_arr[desc_arr.length-1].equals("open."))
            return true;
        return false;
    }
    
    /**
     * It accesses the database to get the data about moving average and the data colected in the same time.
     * 
     * @param vase Vase that we want to analyse.
     * @param sensor Sensor that has the information that we want to analyse.
     * @return List with two maps one with the values of the values for the moving average and the other with the sensor data.
     */
    
    @GetMapping("/movingAverage/{vase}/{sensor}")
    public HashMap<String, List<String>>  getMovingAverage(@PathVariable("vase") int vase, @PathVariable("sensor") String sensor ){
        
        HashMap<String, List<String>> timeAndData = new HashMap<>();
        
        List<String> values = new ArrayList<>();
        List<String> time = new ArrayList<>();
        
        for (SensorData v : storage.getMovingAverage(sensor, vase)){
            time.add(v.getTimestamp().toString());
            values.add(v.getValue().toString());
        }
        
        timeAndData.put("time", time);
        timeAndData.put("values", values);
        
        return timeAndData;
    }
    
    @GetMapping("/values/{vase}/{sensor}/{limit}")
    public List<String> getValues(@PathVariable("vase") int vase, @PathVariable("sensor") String sensor, @PathVariable("limit") int limit){
        List<String> values = new ArrayList<>();
        
        for (SensorData v : storage.getValues(limit, sensor, vase)){
            values.add(v.getValue().toString());
        }
        
        return values;
    }

    @GetMapping("/profiles")
    public HashMap<Long,HashMap<String, Object>> getProfiles(){
        
        HashMap<Long,HashMap<String, Object>> map = new HashMap<>(); 
        for (Profile profile : profile_storage.findAll()) {
            Long profile_id = profile.getId();
            HashMap<String,Object> prof = new HashMap<>();
            prof.put("profile_id", profile_id);
            prof.put("title", profile.getTitle());
            List<HashMap<String,Object>> definitions = new ArrayList<>();
            for (ProfileDefinition p : profile.getDefinitions()){
                HashMap<String,Object> each_def = new HashMap<>();
                each_def.put("def_id", p.getId());
                each_def.put("type", p.getType());
                each_def.put("value", p.getValue());
                each_def.put("above", p.getAbove());
                each_def.put("action", p.getAction());
                definitions.add(each_def);
            }
            prof.put("definitions", definitions);
            List<HashMap<String,Object>> lst = new ArrayList<>();
            for (VaseProfile v : profile.getVases()){
                HashMap<String,Object> each_vase = new HashMap<>();
                each_vase.put("id", v.getId());
                each_vase.put("vase", v.getVase());
                lst.add(each_vase);
            }
            prof.put("vases", lst);
            map.put(profile_id, prof);
        }
        return map;
    }
    
    @RequestMapping(value="/vases_insert")
    public void vases_insert(@RequestParam("vase") String vase, @RequestParam("profile_id") String profile_id){    
        profile_storage.insertVase(vase, profile_id);
    }
    
    @RequestMapping(value="/vases_remove")
    public void vases_remove(@RequestParam("id") String id){    
        profile_storage.removeVase(id);
    }
    
    @RequestMapping(value="/definition_insert")
    public void definition_insert(@RequestParam("above") String above, @RequestParam("type") String type, @RequestParam("action") String action, @RequestParam("value") String value, @RequestParam("profile_id") String profile_id){    
        profile_storage.insertDefinition(Integer.parseInt(above),action,type,value,profile_id);
    }
    
    @RequestMapping(value="/definition_update")
    public void definition_update(@RequestParam("id") String id, @RequestParam("above") String above, @RequestParam("type") String type, @RequestParam("action") String action, @RequestParam("value") String value, @RequestParam("profile_id") String profile_id){    
        profile_storage.updateDefinition(Integer.parseInt(id),Integer.parseInt(above),action,type,value,profile_id);
    }
    
    @RequestMapping(value="/definition_remove")
    public void definition_remove(@RequestParam("id") String id){    
        profile_storage.removeDefinition(id);
    }
    
    @RequestMapping(value="/profile_insert")
    public void profile_insert(@RequestParam("profile") String profile){    
        profile_storage.insertProfile(profile);
    }
}
