package gardenproject.consumer;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer{
    
    @Autowired
    NotificationMap notification_map;
    
    /**
     * Once it gets a new message it places the Notification in the map.
     * 
     * @param notification Its the notification that comes from the kafkastream analyser.
     */
    @KafkaListener(topics = "WARNINGS", groupId = "GardenProject")
    public void consume(String notification) {
        Gson g = new Gson();
        Notification n = g.fromJson(notification, Notification.class);
        notification_map.putNotification(n.getVase(), n);
    }
}
