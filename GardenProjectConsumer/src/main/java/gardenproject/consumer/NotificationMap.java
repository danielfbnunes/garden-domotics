package gardenproject.consumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class NotificationMap {

    private HashMap<Integer, List<Notification>> notification_map = new HashMap<>();
    
    /**
     * Adds a new notification to the list of the vase.
     * 
     * @param vase id of the vase of the notification.
     * @param notification notification to be stored.
     */
    public void putNotification(int vase, Notification notification){
        List<Notification> ln;
        if(notification_map.containsKey(vase)){
           ln = notification_map.get(vase);
           ln.add(notification);
           notification_map.put(vase, ln);
        }
        else{
            ln = new ArrayList<>();
            ln.add(notification);
            notification_map.put(vase, ln);
        }    
    }
    
    /**
     * Returns all present notifications and resets the notifications.
     * 
     * @return Returns all Notifications.
     */
    public Map<Integer, List<Notification>> getNotifications(){
        HashMap<Integer, List<Notification>> temp = notification_map;
        notification_map.clear();
        return temp;
    }
}
