package gardenproject.consumer;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface NotificationsRepository extends CrudRepository<Notification, Long>{
    
    @Query(value = "SELECT n.vase, n.description, n.timestamp, @rownum\\:=@rownum + 1 AS id  FROM notification n,(SELECT @rownum\\:=0) r WHERE n.vase > 0 AND id > ?1+1 ;", nativeQuery = true)
    List<Notification> getNotifications(int vase); 
    
    @Query(value = "SELECT n.vase, n.description, n.timestamp, @rownum\\:=@rownum + 1 AS id  FROM notification n,(SELECT @rownum\\:=0) r WHERE n.vase = -1 AND id > ?1+1 ", nativeQuery = true)
    List<Notification> getDoorNotifications(int vase);
    
    @Query(value = "select * from notification where vase = -1 order by timestamp desc limit 1 ;", nativeQuery = true)
    List<Notification> isDoorOpen();
    
    
}