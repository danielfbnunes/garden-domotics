package gardenproject.consumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "profile")
@JsonIgnoreProperties(allowGetters = true, ignoreUnknown = true)
public class Profile {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String title;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "profile")
    private List<VaseProfile> vases;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "profile")
    private List<ProfileDefinition> definitions;

    public Profile() {
    }

    public Profile(Long id, String title, List<VaseProfile> vases, List<ProfileDefinition> definitions) {
        this.id = id;
        this.title = title;
        this.vases = vases;
        this.definitions = definitions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<VaseProfile> getVases() {
        return vases;
    }

    public void setVases(List<VaseProfile> vases) {
        this.vases = vases;
    }

    public List<ProfileDefinition> getDefinitions() {
        return definitions;
    }

    public void setDefinitions(List<ProfileDefinition> definitions) {
        this.definitions = definitions;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.id);
        hash = 73 * hash + Objects.hashCode(this.title);
        hash = 73 * hash + Objects.hashCode(this.vases);
        hash = 73 * hash + Objects.hashCode(this.definitions);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Profile other = (Profile) obj;
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.vases, other.vases)) {
            return false;
        }
        if (!Objects.equals(this.definitions, other.definitions)) {
            return false;
        }
        return true;
    }
    
    
}
