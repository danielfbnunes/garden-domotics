package gardenproject.consumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "profile_definition")
@JsonIgnoreProperties(allowGetters = true, ignoreUnknown = true)
public class ProfileDefinition {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String type;
    private Double value;
    private Boolean above;
    private String action;
    @ManyToOne
    private Profile profile;

    public ProfileDefinition() {
    }

    public ProfileDefinition(Long id, String type, Double value, Boolean above, String action, Profile profile) {
        this.id = id;
        this.type = type;
        this.value = value;
        this.above = above;
        this.action = action;
        this.profile = profile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Boolean getAbove() {
        return above;
    }

    public void setAbove(Boolean above) {
        this.above = above;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
