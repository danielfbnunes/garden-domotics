package gardenproject.consumer;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
    
    @Query(value = "select * from profile_definition ;", nativeQuery = true)
    public List<ProfileDefinition> getProfileDefinitions();
    
    @Query(value = "select profile.id, profile.title from profile join vase_profile where vase = ?1 and profile.id = profile_id ;", nativeQuery = true)
    public List<Profile> getVaseProfile(int vase);
    
    @Modifying
    @Query(value = "insert into vase_profile(vase,profile_id) values (:v,:p)", nativeQuery = true)
    @Transactional
    public void insertVase(@Param("v") String vase, @Param("p") String profile);

    @Modifying
    @Query(value = "delete from vase_profile where id = :id", nativeQuery = true)
    @Transactional
    public void removeVase(@Param("id") String id);
    
    @Modifying
    @Query(value = "insert into profile_definition(above,action,type,value,profile_id) values (:above,:action,:type,:value,:profile_id)", nativeQuery = true)
    @Transactional
    public void insertDefinition(@Param("above") int above, @Param("action") String action, @Param("type") String type, @Param("value") String value, @Param("profile_id") String profile_id);
    
    @Modifying
    @Query(value = "insert into profile_definition(id,above,action,type,value,profile_id) values (:id,:above,:action,:type,:value,:profile_id) on duplicate key update above=values(above),action=values(action),type=values(type),value=values(value),profile_id=values(profile_id)", nativeQuery = true)
    @Transactional
    public void updateDefinition(@Param("id") int id, @Param("above") int above, @Param("action") String action, @Param("type") String type, @Param("value") String value, @Param("profile_id") String profile_id);
    
    @Modifying
    @Query(value = "delete from profile_definition where id = :id", nativeQuery = true)
    @Transactional
    public void removeDefinition(@Param("id") String id);
    
    @Modifying
    @Query(value = "insert into profile(title) values (:p)", nativeQuery = true)
    @Transactional
    public void insertProfile(@Param("p") String profile);
}