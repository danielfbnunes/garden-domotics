package gardenproject.consumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "sensor_data_history")
@JsonIgnoreProperties(value = {"timestamp"}, allowGetters = true, ignoreUnknown = true)
public class SensorData implements Serializable {

    @Id
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date timestamp;
    private int vase;
    private String type; // Temperature, Humidity, etc.
    private Double value; // ^^^ Corresponding value.

    public SensorData() {}

    public SensorData(Long id, Date timestamp, String type, Double value) {
        this.id = id;
        this.timestamp = timestamp;
        this.type = type;
        this.value = value;
    }

    public int getVase() {
        return vase;
    }

    public void setVase(int vase) {
        this.vase = vase;
    }

    
    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Notification{ timestamp=" + timestamp + ", vase=" + vase + ", type=" + type + ", value=" + value + '}';
    }
}
