package gardenproject.consumer;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StatesRepository extends CrudRepository<SensorData, Long>{
    
    /**
     * Gets the current state of all vases in the repository.
     * @return List with the SensorData corresponding to the query.
     */
    @Query(value = "select data1.id, data1.vase, data1.type, data1.timestamp, data1.value from sensor_data_history as data1 INNER JOIN (select vase, type, max(timestamp) as timestamp from sensor_data_history group by vase, type) as data2 ON data1.type = data2.type and data1.timestamp = data2.timestamp ;", nativeQuery = true)
    public List<SensorData> getAllStates();
    
    /**
     * Gets all the states of the specific vase in the repository.
     * 
     * @param vase id of the vase that the information is about.
     * @return List with all the SensorData corresponding to the query.
     */
    @Query(value = "select * from sensor_data_history WHERE vase = ?1 ;", nativeQuery = true)
    List<SensorData> getHistory(int vase);
    
    /**
     * Gets the history values ordered by date and limited to a certain number.
     * 
     * @param limit Number of results we want returned
     * @param sensor Name of the sensor to use in the query.
     * @param vase Id of the vase to use in the query.
     * @return List with all the SensorData corresponding to the query;
     */
    @Query(value = "select * from sensor_data_history where type = ?2 and vase = ?3 order by timestamp asc limit ?1 ;", nativeQuery = true)
    List<SensorData> getValues(int limit, String sensor, int vase );
    
    /**
     * Gets the data about moving average of a specific vase and sensor.
     * 
     * @param sensor Name of the sensor to use in the query.
     * @param vase Id of the vase to use in the query.
     * @return List with all the SensorData corresponding to the query.
     */
    @Query(value = "select * from moving_average_history where type = ?1 and vase = ?2 order by timestamp ASC ;", nativeQuery = true)
    List<SensorData> getMovingAverage(String sensor, int vase);
    
}