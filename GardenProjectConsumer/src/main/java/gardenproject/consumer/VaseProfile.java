package gardenproject.consumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vase_profile")
@JsonIgnoreProperties(allowGetters = true, ignoreUnknown = true)
public class VaseProfile implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private Integer vase;
    @ManyToOne
    private Profile profile;

    public VaseProfile() {
    }

    public VaseProfile(Long id, Integer vase, Profile profile) {
        this.id = id;
        this.vase = vase;
        this.profile = profile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVase() {
        return vase;
    }

    public void setVase(Integer vase) {
        this.vase = vase;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}