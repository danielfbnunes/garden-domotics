/*
    Function used to representate the notifications
	Once its caled a popup apears.
*/
function popup(message) {
    var popup = '<div id="popup" class="alert alert-primary"\
        style="color: #004085; font-size: 25px; background-color: #cce5ff; width: 400px; position: fixed; right: 0; z-index:999; margin: 3% 3% 0 0; opacity: 1">\
        <button type="button" class="close" data-dismiss="alert">&times;</button>'+ message + '\
        </div>';
	document.getElementsByTagName('body')[0].insertAdjacentHTML('afterbegin', popup);
	setTimeout(function () {
	    var interval = setInterval(function () {
		    var popup = document.getElementById("popup")
			if (popup.style.opacity <= 0.1) {
			    popup.parentElement.removeChild(popup)
				clearInterval(interval)
			}
			popup.style.opacity -= 0.025
		}, 100)
		document.getElementById("popup").onmouseover = function () {
			document.getElementById("popup").style.opacity = 1
			clearInterval(interval)
		}
		document.getElementById("popup").onmouseout = function () {
			interval = setInterval(function () {
				var popup = document.getElementById("popup")
				if (popup.style.opacity <= 0.1) {
					popup.parentElement.removeChild(popup)
					clearInterval(interval)
				}
				popup.style.opacity -= 0.025
		    }, 50)
		}
	}, 1000)
}
/*
	Periodic function responsible for getting notifications.
*/
setInterval(function() {
    $.ajax({ 
		type: 'GET', 
		url: 'http://localhost:8080/api/notification', 
		headers: {'Access-Control-Allow-Origin': 'http://localhost:8080'}, 
		data: { get_param: 'value' },
		dataType: 'json',
		success: function (data) { 
			console.log("QUERY");
			popup(JSON.stringify(data));
		}
	});
}, 5000)