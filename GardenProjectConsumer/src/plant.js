function getPlant(){
    $.ajax({ 
        type: 'GET', 
        url: 'http://localhost:8080/api/plants/'+ (parseInt(window.location.href.split("#")[1])), 
        headers: {'Access-Control-Allow-Origin': 'http://localhost:8080'}, 
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (element) {
            t_body = document.getElementById("t_body");
            var tr = document.createElement("tr");
            var td = document.createElement("td");
            var node = document.createTextNode(element.vase);
            td.appendChild(node);
            tr.appendChild(td);
                
            td = document.createElement("td");
            node = document.createTextNode(element.plant_type);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.temperature);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.humidity);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.light);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.co2);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.soil_ph);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            node = document.createTextNode(element.soil_humidity);
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            if(element.light_on){
                node = document.createTextNode("On");
            }
            else{
                node = document.createTextNode("Off");
            }
            td.appendChild(node);
            tr.appendChild(td);

            td = document.createElement("td");
            if(element.blower_on){
                node = document.createTextNode("On");
            }
            else{
                node = document.createTextNode("Off");
            }
            td.appendChild(node);
            tr.appendChild(td);

            t_body.appendChild(tr);
        }
    });
}

function ligarLuz(){
    $.ajax({ 
    type: 'GET', 
    url: 'http://localhost:8080/api/light/'+(window.location.href.split("#")[1] - 1), 
    headers: {'Access-Control-Allow-Origin': 'http://localhost:8080'}, 
    })
    t_body = document.getElementById("t_body").deleteRow(0);
    getPlant();
}

function ligarVent(){
    $.ajax({ 
    type: 'GET', 
    url: 'http://localhost:8080/api/blower/'+(window.location.href.split("#")[1] - 1), 
    headers: {'Access-Control-Allow-Origin': 'http://localhost:8080'}, 
    })
    t_body = document.getElementById("t_body").deleteRow(0);
    getPlant();
}

function refresh(){
    t_body = document.getElementById("t_body").deleteRow(0);
    getPlant();
}

function getChart(){
    var ctx = document.getElementById("myChart").getContext('2d');
    var time;
    var values;
    $.ajax({ 
        type: 'GET', 
        url: 'http://localhost:8080/api/temperatureMovingAverage/1/temperature', 
        headers: {'Access-Control-Allow-Origin': 'http://localhost:8080'}, 
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (data) {
            var dataset = new Array();
            var label = new Array();
            var color = ["red", "blue"]
            var titles = ["Temperature Moving Average", "Temperature Data"]
            $.each(data, function(index, element) {
                label = element.time;
                data = {
                    label: titles[index],
                    fill: false,
                    borderColor: color[index],
                    data : element.values,
                    borderWidth: index+1,
                    pointBackgroundColor :color[index] ,
                    pointRadius : index * 3
                }
                dataset.push(data);
            });
            var lineChart = new Chart(ctx, {
                type: 'line',
                data: { 
                    labels: label,
                    xAxisID : "Time",
                    yAxisID : "Value",
                    datasets : dataset
                }, 
                options:{}

            });
        }
    });
}

getPlant();
getChart();