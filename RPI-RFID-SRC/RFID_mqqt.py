import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
from py532lib.i2c import *
from py532lib.frame import *
from py532lib.constants import *

MQTT_BROKER_IP_ADDRESS = "197.168.0.74"
MQTT_BROKER_PORT = 1883
MQTT_KEEP_ALIVE = 60


def main():
    """ This is the main function that contains
        the lifecycle of the rpi.
        It reads the data from the card swiped at the PN532 RFID module.
        And if its uid its equal to the pretended uid it will send a message
        to the topic openDoor using mqtt. 
    """
    # Configure mqtt client.
    mqttc = mqtt.Client()
    mqttc.connect(MQTT_BROKER_IP_ADDRESS,MQTT_BROKER_PORT,MQTT_KEEP_ALIVE)
    mqttc.loop_start()
    # Configure the PN532 module.
    pn532 = Pn532_i2c()
    pn532.SAMconfigure()

    while True:
        # Block until card swipe.
        card_data = pn532.read_mifare().get_data()
        # Check if card as the correct uid.
        if len(card_data) > 15 and card_data[6:-4]==b'\x04\x01\x02\x03\x04\x05':
            print("Door Open!")
            # Publish message.
            mqttc.publish("openDoor", "1")

    mqttc.loop_stop()
    mqttc.disconnect()

if __name__ == "__main__":
    main()
