import time
# Led
import RPi.GPIO as GPIO
# Json
import json
# MQTT
import paho.mqtt.client as paho
# Others
import random
# RFID
from py532lib.i2c import *
from py532lib.frame import *
from py532lib.constants import *

# Read data from properties
property_dict = {}
f = open("rasp_properties", "r")
for line in f:
    prop = line.split("=")[0].strip().replace(" ", "")
    value = line.split("=")[1].strip().replace(" ", "")
    property_dict[prop] = value
    
#print(property_dict)

#define callback for receieved messages
def on_message(client, userdata, message):
    time.sleep(1)
    print("received message =",str(message.payload.decode("utf-8")))

# conect to broker
#define constants
publish_topic = property_dict["PUBLISH_TOPIC"]
broker = property_dict["BROKER_IP"] 
port = int(property_dict["BROKER_PORT"])
user = property_dict["BROKER_USERNAME"] 
password = property_dict["BROKER_PASSWORD"] 

# create client
client= paho.Client("Vase" + property_dict["VASE"])
#client.username_pw_set(user, password=password)
client.on_message=on_message
#####
print("Connecting to broker: ", broker)
client.connect(broker, port)

#start loop to process received messages
client.loop_start() 

#subscribe to topic
client.subscribe(subscribe_topic)
   
# loop for reading data from all sensors
while 1:

    pn532 = Pn532_i2c()
    pn532.SAMconfigure()

    while True:
        # Block until card swipe.
        card_data = pn532.read_mifare().get_data()
        # Check if card as the correct uid.
        if len(card_data) > 15 and card_data[6:-4]==b'\x04\x01\x02\x03\x04\x05':
            print("Door Open!")
            # Publish message.
            client.publish(publish_topic, "{\"vase\":-1, \"description\":\"The door has been opened.\"}")#publish

#disconnect from broker
client.disconnect() 
client.loop_stop()
