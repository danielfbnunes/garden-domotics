package com.domotics.SpringKafkaStreams;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class DataProcessor {
    
    @Autowired
    private SensorDataRepository sensorDataPersistence;
    @Autowired
    private MovingAverageRepository movingAveragePersistence;
    @Autowired
    private NotificationRepository notificationPersistence;


    public DataProcessor() {}
    
    public void saveSensorData(String data) {
        
        Gson gson = new Gson();
        SensorData n = gson.fromJson(data, SensorData.class);
        n.setTimestamp(Calendar.getInstance().getTime());
        System.out.println("GSON -> "+n.toString());
        sensorDataPersistence.save(n);
    }
    
    public void saveMovingAverage(String data) {
        
        Gson gson = new Gson();
        MovingAverage m = gson.fromJson(data, MovingAverage.class);
        m.setTimestamp(Calendar.getInstance().getTime());
        System.out.println("GSON -> "+m);
        movingAveragePersistence.save(m);
    }
    
    public void saveNotification(String data) {
        if (data == null) return;
        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();   
        Notification n = gson.fromJson(data, Notification.class);
        n.setTimestamp(Calendar.getInstance().getTime());
        System.out.println("GSON -> "+n);
        notificationPersistence.save(n);
    }
}
