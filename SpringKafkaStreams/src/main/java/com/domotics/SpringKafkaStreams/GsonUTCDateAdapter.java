package com.domotics.SpringKafkaStreams;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class GsonUTCDateAdapter implements JsonSerializer<Date>,JsonDeserializer<Date> {

    private final DateFormat dateFormat;

    public GsonUTCDateAdapter() {
      dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);      
      dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));                               
    }

    @Override
    public JsonElement serialize(Date t, java.lang.reflect.Type type, JsonSerializationContext jsc) {
        return new JsonPrimitive(dateFormat.format(t));
    }

    @Override
    public Date deserialize(JsonElement je, java.lang.reflect.Type type, JsonDeserializationContext jdc) throws JsonParseException {
        try {
            return dateFormat.parse(je.getAsString());
        } catch (ParseException e) {
            throw new JsonParseException(e);
        }
    }
}
