/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domotics.SpringKafkaStreams;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "moving_average_history")
@JsonIgnoreProperties(value = {"timestamp"}, allowGetters = true, ignoreUnknown = true)
public class MovingAverage implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private Date timestamp;
    private int vase;
    private String type;
    private Double value;

    public MovingAverage() { }

    public MovingAverage(Long id, Date timestamp, int vase, String type, Double value) {
        this.id = id;
        this.timestamp = timestamp;
        this.vase = vase;
        this.type = type;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
        public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
    
    
    public int getVase() {
        return vase;
    }

    public void setVase(int vase) {
        this.vase = vase;
    }

    @Override
    public String toString() {
        return "{" + "\"id\":" + id + ", \"timestamp\":" + timestamp + ", \"vase\":" + vase + ", \"type\":\"" + type + "\", \"value\":" + value + '}';
    }
    
}