package com.domotics.SpringKafkaStreams;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notification")
@JsonIgnoreProperties(value = {"timestamp"}, allowGetters = true, ignoreUnknown = true)
public class Notification {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private int vase;
    private Date timestamp;
    private String description;

    public Notification(int vase, Date timestamp, String description) {
        this.vase = vase;
        this.timestamp = timestamp;
        this.description = description;
    }

    public int getVase() {
        return vase;
    }

    public void setVase(int vase) {
        this.vase = vase;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);      
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));   
        return "{" + "\"vase\":" + vase + ", \"timestamp\":\"" + dateFormat.format(timestamp) + "\", \"description\":\"" + description + "\"}";
    }
}
