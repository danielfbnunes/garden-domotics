package com.domotics.SpringKafkaStreams;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
    
    @Query(value = "select * from vase_profile ;", nativeQuery = true)
    public List<VaseProfile> getVaseProfiles();
    
    @Query(value = "select * from profile_definition ;", nativeQuery = true)
    public List<ProfileDefinition> getProfileDefinitions();
}