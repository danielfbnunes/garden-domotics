package com.domotics.SpringKafkaStreams;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduleTaskProfileUpdate {
    public Map<Integer, List<ProfileDefinition>> profile_definitions = new HashMap<>();
    
    @Autowired
    ProfileRepository profilesRep;
    
    @Scheduled(fixedRate = 1000)
    public void update_profiles() {
        List<Profile> ps = profilesRep.findAll();
        for (Profile p: ps) {
            p.getVases().forEach(v -> profile_definitions.put(v.getVase(), p.getDefinitions()));
        }
        /*List<VaseProfile> vase_profiles = profilesRep.getVaseProfiles();
        List<ProfileDefinition> profileDefinitions = profilesRep.getProfileDefinitions();
        for (VaseProfile vp : vase_profiles) {
            for (ProfileDefinition pd : profileDefinitions) {
                if (vp.getProfile().equals(pd.getProfile())) {
                    profile_definitions.put(vp.getVase(), pd);
                    break;
                }
            }
        }*/
    }
    
}
