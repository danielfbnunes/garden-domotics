package com.domotics.SpringKafkaStreams;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.kstream.internals.WindowedDeserializer;
import org.apache.kafka.streams.kstream.internals.WindowedSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

/*@EnableDiscoveryClient*/
@SpringBootApplication
@EnableJpaAuditing
@EnableScheduling
public class SpringKafkaStreamsApplication {
    
        @Autowired
        private DataProcessor dataProcessor;
        @Autowired
        private ScheduleTaskProfileUpdate profileUpdate;
        @Autowired
        ProfileRepository profilesRep;
        @Autowired
        VaseProfileRepository vaseProfilesRep;
        @Autowired
        ProfileDefinitionRepository profileDefinitionRep;
        @Autowired
        private DiscoveryClient discoveryClient;
        private Map<String, Double> oldValues = new HashMap<>();
        private double alpha = 0.1;
        private static final long TEMPERATURE_WINDOW_SIZE = 30L;
        
        public boolean check_notification(SensorData sd) {
            if (profileUpdate.profile_definitions.containsKey(sd.getVase())) {
                List<ProfileDefinition> pds = profileUpdate.profile_definitions.get(sd.getVase());
                for (ProfileDefinition pd : pds)
                    if (pd.getType().equals(sd.getType()) && 
                        (pd.getAbove() && (sd.getValue() > pd.getValue() ||
                        sd.getValue() < pd.getValue()))) {
                        send_action(sd.getVase(), pd.getAction());
                        return true;
                    }
            }
            return false;
        }
        
        public Notification map_to_notification(SensorData sd) {
            if (profileUpdate.profile_definitions.containsKey(sd.getVase())) {
                List<ProfileDefinition> pds = profileUpdate.profile_definitions.get(sd.getVase());
                for (ProfileDefinition pd : pds)
                    if (pd.getType().equals(sd.getType())) {
                        if ((pd.getAbove() && sd.getValue() > pd.getValue()) || (!pd.getAbove() && sd.getValue() < pd.getValue()))
                            return new Notification(sd.getVase(), sd.getTimestamp(), 
                                sd.getType()+" is "+(pd.getAbove() ? "above " : "below ")+pd.getValue()+" ("+sd.getValue()+").");
                    }
            }
            return null;
        }
        
        public void send_action(int vase, String action) {
            //List<ServiceInstance> instances = discoveryClient.getInstances("garden-rest");
            //if (instances.isEmpty()) return;
            //String url = instances.get(0).getHost()+":"+instances.get(0).getPort()+"/api/";
            String url = "http://deti-engsoft-05.ua.pt:8080/api";
            System.out.println(vase+" => "+action);
            switch (action) {
                case "turn light on":
                    url += "/light/"+vase+"/on";
                    break;
                case "turn light off":
                    url += "/light/"+vase+"/off";
                    break;
                case "turn blower on":
                    url += "/light/"+vase+"/off";   
                    break;
                case "turn blower off":
                    url += "/light/"+vase+"/off";
                    break;
                default:
                    return;
            }
            System.out.println("SENDING HTTP REQ. TO "+url);
            URL obj;
            try {
                obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.getResponseCode();
            } catch (Exception ex) {}
        }
        
        @EventListener(ApplicationReadyEvent.class)
        public void start_processing() {
            Properties props = new Properties();
            props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-pipe");
            props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "deti-engsoft-01.ua.pt:9092");
            props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            
            final StreamsBuilder builder = new StreamsBuilder();
            KStream<String, String> source = builder.<String, String>stream("sensors-data100");
            KStream<String, String> source_door = builder.<String, String>stream("door-notifications2");
            

            source.filter((k, v) -> {
                    Gson g = new Gson();
                    SensorData n = g.fromJson(v, SensorData.class);
                    return n != null;
                })
                .map((k, v) -> {
                    //v = new String(v.substring(1, v.length()-1));
                    Gson g = new Gson();
                    SensorData n = g.fromJson(v, SensorData.class);
                    //n.setTimestamp(Calendar.getInstance().getTime());
                    double oldValue = oldValues.getOrDefault(n.getType(), 0.0);
                    oldValue = alpha * n.getValue() +(1 - alpha) * oldValue;
                    oldValues.put(n.getType(), oldValue);
                    n.setValue(oldValue);
                    return new KeyValue<>(k, n.toString());
                })
                .peek((k, v) -> {
                    dataProcessor.saveMovingAverage(v);
                })
                .filter((k, v) -> {
                    Gson g = new Gson();
                    SensorData n = g.fromJson(v, SensorData.class);
                    if (n != null)
                        n.setType(n.getType()+"_moving_average");
                    return n != null &&  check_notification(n);
                })
                .mapValues(v -> {
                    Gson g = new Gson();
                    SensorData n = g.fromJson(v, SensorData.class);
                    n.setTimestamp(Calendar.getInstance().getTime());
                    Notification notification = map_to_notification(n);
                    return notification != null ? notification.toString() : "{}";
                })
                .to("notifications_g33");
            
            source.filter((k, v) -> {
                    Gson g = new Gson();
                    SensorData n = g.fromJson(v, SensorData.class);
                    return n != null;
            })
            .map((k, v) -> {
                //v = new String(v.substring(1, v.length()-1));
                Gson g = new Gson();
                SensorData n = g.fromJson(v, SensorData.class);
                //n.setTimestamp(Calendar.getInstance().getTime());
                return new KeyValue<>(k, n.toString());
            })
            .peek((k, v) -> {
                dataProcessor.saveSensorData(v);
            })
            .filter((k, v) -> {
                Gson g = new Gson();
                SensorData n = g.fromJson(v, SensorData.class);
                return check_notification(n);
            })
            .mapValues(v -> {
                Gson g = new Gson();
                SensorData n = g.fromJson(v, SensorData.class);
                Notification notification = map_to_notification(n);
                if (notification == null) return "{}";
                notification.setTimestamp(Calendar.getInstance().getTime());
                return notification != null ? notification.toString() : "{}";
            })
            .peek((k, v) -> {
                dataProcessor.saveNotification(v);
            }).map((k, v) -> {
                //v = new String(v.substring(1, v.length()-1));
                Gson g = new Gson();
                SensorData n = g.fromJson(v, SensorData.class);
                n.setTimestamp(Calendar.getInstance().getTime());
                return new KeyValue<>(k, n.toString());
            })
            .to("notifications_g33");
            
            final KStream<Windowed<String>, String> windowing = //builder3.<String, String>stream("sensors-data")
            source
            .selectKey((key, value) -> "temp")
            .groupByKey()
            .windowedBy(TimeWindows.of(TEMPERATURE_WINDOW_SIZE*1000))
            .reduce((value1, value2) -> {
                //value2 = new String(value2.substring(1, value2.length()-1));
                Gson g = new Gson();
                SensorData n1 = g.fromJson(value1, SensorData.class);
                SensorData n2 = g.fromJson(value2, SensorData.class);
                n2.setValue(Math.abs(n2.getValue() - n1.getValue()));
                //n2.setTimestamp(Calendar.getInstance().getTime());
                return n2.toString();
            })
            .filter((k, v) -> {
                Gson g = new Gson();
                SensorData n = g.fromJson(v, SensorData.class);
                n.setType(n.getType()+"_window");
                return check_notification(n);
            })
            .mapValues(v -> {
                Gson g = new Gson();
                SensorData n = g.fromJson(v, SensorData.class);
                n.setTimestamp(Calendar.getInstance().getTime());
                Notification notification = map_to_notification(n);
                return n != null ? notification.toString() : "{}";
            })
            .toStream()
            .peek((k, v) -> {
                dataProcessor.saveNotification(v);
            });
            //.peek((k, v) -> System.out.println("Windowind"+v));
            final WindowedSerializer<String> windowedSerializer  = new WindowedSerializer<>(new StringSerializer());
            final WindowedDeserializer<String> windowedDeserializer  = new WindowedDeserializer<>(new StringDeserializer());
            //windowing.to("notifications_g33", Produced.with(Serdes.serdeFrom(windowedSerializer, windowedDeserializer), Serdes.String()));

            
            source_door.mapValues(v -> {
                Gson g = new Gson();
                Notification notification = g.fromJson(v, Notification.class);
                if (notification == null) return "{}";
                notification.setTimestamp(Calendar.getInstance().getTime());
                return notification != null ? notification.toString() : "{}";
            }).peek((k, v) -> dataProcessor.saveNotification(v));
            
            final Topology topology = builder.build();
            final KafkaStreams streams = new KafkaStreams(topology, props);
            final CountDownLatch latch = new CountDownLatch(1);

            // attach shutdown handler to catch control-c
            Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
                @Override
                public void run() {
                    streams.close();
                    latch.countDown();
                }
            });

            try {
                streams.start();
                latch.await();
            } catch (Throwable e) {
                System.exit(1);
            }
            System.exit(0);
        }
    
	public static void main(String[] args) {
            SpringApplication.run(SpringKafkaStreamsApplication.class, args);
        }
}
