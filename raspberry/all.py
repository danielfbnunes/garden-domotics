# DHT11
import time
import Adafruit_DHT
 # PhotoResistor
from gpiozero import LightSensor
import time
# Led
import RPi.GPIO as GPIO
# Json
import json
# MQTT
import paho.mqtt.client as paho
# Others
import random
#Numpy
import numpy as np


# Read data from properties
property_dict = {}
f = open("rasp_properties", "r")
for line in f:
    prop = line.split("=")[0].strip().replace(" ", "")
    value = line.split("=")[1].strip().replace(" ", "")
    property_dict[prop] = value
    
#print(property_dict)

# DHT11 configuration - temperature and humidity
dht11_sensor=Adafruit_DHT.DHT11
dht11_gpio=int(property_dict["DHT11_gpio"])
humidity = temperature = 0

#PhotoResistor configuration
#conected to pin 4
photoresistor = LightSensor(int(property_dict["PHOTORESISTOR_gpio"]))

# Led configuration
# conected to pin 18
led_pin = int(property_dict["LED_gpio"])
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(led_pin, GPIO.OUT)

#ph 
ph_gpio = int(property_dict["PH_gpio"])

#soil_humidity
soil_humidity_gpio = int(property_dict["SOIL_HUMIDITY_gpio"])

#co2
co2_gpio = int(property_dict["CO2_gpio"])


#define callback for receieved messages
def on_message(client, userdata, message):
    time.sleep(1)
    #print("received message =",str(message.payload.decode("utf-8")))
    msg = str(message.payload.decode("utf-8"))
    json_data = json.loads(msg) 
    print(msg)
    print(json_data)
    i=json_data["id"]
    t=json_data["type"]
    a=json_data["action"]
    
    if (str(i)==str(property_dict["VASE"])) and ("light_on" == t+"_" +a):
        print("Turning the light on...")
        GPIO.setup(int(property_dict["LIGHT_PIN"]),  GPIO.OUT)
        GPIO.output(int(property_dict["LIGHT_PIN"]),  GPIO.HIGH)
        print("Ok!!")

    if (str(i)==str(property_dict["VASE"])) and ("light_off" == t+"_" +a):
        print("Turning the light off...")
        GPIO.setup(int(property_dict["LIGHT_PIN"]),  GPIO.OUT)
        GPIO.output(int(property_dict["LIGHT_PIN"]),  GPIO.LOW)
        print("Ok!!")

    if (str(i)==str(property_dict["VASE"])) and ("blower_on" == t+"_" +a):
        print("Turning the fan on...")
        GPIO.setup(int(property_dict["BLOWER_PIN"]) , GPIO.OUT)
        GPIO.output(int(property_dict["BLOWER_PIN"]) , GPIO.HIGH)
        print("Ok!!")

    if (str(i)==str(property_dict["VASE"])) and ("blower_off" == t+"_" +a):
        print("Turning the fan off...")
        GPIO.setup(int(property_dict["BLOWER_PIN"]) , GPIO.OUT)
        GPIO.output(int(property_dict["BLOWER_PIN"]) , GPIO.LOW)
        print("Ok!!")





# conect to broker
#define constants
publish_topic = property_dict["PUBLISH_TOPIC"]
subscribe_topic = property_dict["SUBSCRIBE_TOPIC"]
broker = property_dict["BROKER_IP"] 
port = int(property_dict["BROKER_PORT"])
user = property_dict["BROKER_USERNAME"] 
password = property_dict["BROKER_PASSWORD"] 

# create client
client= paho.Client("Vase" + property_dict["VASE"])
#client.username_pw_set(user, password=password)
client.on_message=on_message
#####
print("Connecting to broker: ", broker)
client.connect(broker, port)
print("Connected to broker: " , broker)
#if able to connect turn the led on:
GPIO.output(led_pin,GPIO.HIGH)

#start loop to process received messages
client.loop_start() 

#subscribe to topic
client.subscribe(subscribe_topic)




   
# loop for reading data from all sensors
while 1:

        # Building the json to be send to the broker
        json_data_temp = {}
        json_data_temp['type'] = "temperature"
        json_data_co2 = {}
        json_data_co2['type'] = "co2"
        json_data_hum = {}
        json_data_hum['type'] = "humidity"
        json_data_light = {}
        json_data_light['type'] = "luminosity"
        json_data_soil_hum = {}
        json_data_soil_hum['type'] = "soil_humidity"
        json_data_ph = {}
        json_data_ph['type'] = "soil_ph"

        # add vase
        json_data_temp['vase'] = int(property_dict["VASE"])
        json_data_co2['vase'] = int(property_dict["VASE"])
        json_data_hum['vase'] = int(property_dict["VASE"])
        json_data_light['vase'] = int(property_dict["VASE"])
        json_data_soil_hum['vase'] = int(property_dict["VASE"])
        json_data_ph['vase'] = int(property_dict["VASE"])

        ######################
        # Regarding DHT11    #
        ######################

        # get a temperature/humidity reading (waiting 2 seconds between each retry).
        humidity, temperature = Adafruit_DHT.read_retry(dht11_sensor, dht11_gpio)
        
        # the Pi might fail to get a valid reading. So check if readings are valid.
        if  humidity is not None and temperature is not None:
            json_data_temp['value'] = temperature
            json_data_hum['value'] = humidity
            #print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))
        else:
            print('Failed to get readings from DHT11. Try again!')
            json_data_temp['value'] = None
            json_data_hum['value'] = None


        ###########################
        # Regarding photoresistor #
        ###########################
        
        #print(photoresistor.value)
        json_data_light['value'] = photoresistor.value

        #####################
        # Regarding pH      #
        #####################
        ph = np.random.normal(9,1, 1)[0]
        json_data_ph['value'] = ph

        ###########################
        # Regarding soil_humidiy  #
        ###########################
        soil_humidity = np.random.normal(30,5, 1)[0]
        json_data_soil_hum['value'] = soil_humidity

        ##################
        # Regarding co2  #
        ##################
        co2 = np.random.normal(20,3, 1)[0]
        json_data_co2['value'] = co2

        built_json_temp = json.dumps(json_data_temp)
        built_json_co2 = json.dumps(json_data_co2)
        built_json_ph = json.dumps(json_data_ph)
        built_json_light = json.dumps(json_data_light)
        built_json_soil_hum = json.dumps(json_data_soil_hum)
        built_json_hum = json.dumps(json_data_hum)
        # publish results
        client.publish(publish_topic, built_json_temp)#publish
        print("\n",json_data_temp)
        client.publish(publish_topic, built_json_co2)#publish
        print("\n",json_data_co2)
        client.publish(publish_topic, built_json_ph)#publish
        print("\n",json_data_ph)
        client.publish(publish_topic, built_json_light)#publish
        print("\n",json_data_light)
        client.publish(publish_topic, built_json_soil_hum)#publish
        print("\n",json_data_soil_hum)
        client.publish(publish_topic, built_json_hum)#publish
        print("\n",json_data_hum)
        time.sleep(5)
        

#disconnect from broker
client.disconnect() 
client.loop_stop()
