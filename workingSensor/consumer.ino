#include <ESP8266WiFi.h>
#include <PubSubClient.h>
 
const char* ssid = "Redmi";
const char* password =  "80e43269503e";
const char* mqttServer = "192.168.43.100";
const int mqttPort = 1883;
const char* mqttUser = "username";
const char* mqttPassword = "password";
int light_led  = D7;
 
WiFiClient espClient;
PubSubClient client(espClient);
 
void setup() {
 
  Serial.begin(115200);
  pinMode(light_led, OUTPUT);
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
 
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
 
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP8266Client", mqttUser, mqttPassword )) {
 
      Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
  }
 
  client.publish("testTopic", "Hello from ESP8266");
  client.subscribe("testTopic2");
 
}
 
void callback(char* topic, byte* payload, unsigned int length) {
 
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
  String temp = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    temp.concat((char)payload[i]);
  }
   String message(temp);
 
  if(message.equals("turn_light_on"))
  {
    Serial.println("Turning the light on...");
    digitalWrite(light_led, HIGH);   
    //turn_light_on();
  }

  if(message.equals("turn_light_off"))
  {
    Serial.println("Turning the light off...");
    digitalWrite(light_led, LOW);   
    //turn_light_on();
  }
  Serial.println("-----------------------");
 
}
 
void loop() {
  client.loop();
}

/*
void turn_light_on() {
  digitalWrite(8, HIGH); // Turn the LED on
}
*/
