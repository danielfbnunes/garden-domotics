  #include <SimpleDHT.h> //  DHT.h library
  #include <ESP8266WiFi.h>
  #include <PubSubClient.h>


  const char* ssid = "Redmi";
  const char* password = "80e43269503e";
  char* topic = "testTopic";
  char* topic2 = "testTopic2";
  char* mqtt_server = "192.168.43.203";

  
  int pinDHT11 = D6;
  SimpleDHT11 dht11;
 int soundDetectedPin = D5;
 int soundDetectedVal = HIGH;
  boolean bAlarm = false;
  unsigned long lastSoundDetectTime; // Record the time that we measured a sound
  int soundAlarmTime = 500; // Number of milli seconds to keep the sound alarm high
  WiFiClient espClient; 
PubSubClient client(espClient);
long lastMsg = 0;
char msg[200];
int value = 0;

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      // client.publish(topic, "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

  void setup() {
    // Initialize sensor
    Serial.begin(115200);
    //pinMode(D0,OUTPUT);
    pinMode(A0, INPUT);
    pinMode (soundDetectedPin, INPUT) ; // input from the Sound Detection Module

    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");  
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());


    Serial.begin(115200);
    client.setServer(mqtt_server, 1883);
    client.setCallback(callback);
    client.publish("testTopic", "Hello from ESP8266");
    client.subscribe("testTopic2");
    }
  
  void loop() 
  {
    if (!client.connected()) {
    reconnect();
    }

     client.loop();
    //Initialize variables
   Serial.print("[LATEST VALUES]\n");
    
   byte temperature = 0;
   byte humidity = 0;
   
   dht11.read(pinDHT11, &temperature, &humidity, NULL);
    int brightness = analogRead(A0);
   Serial.print("Temperatura: ");
   Serial.println(temperature);
   Serial.print("Humidade: ");
   Serial.println(humidity);
   Serial.print("Brightness: ");
   Serial.println(brightness);
  
   String temp = "Temperature: " + String(temperature) + "\t";
   temp += "Humidity: " + String(humidity)+ "\t";
   temp += "Brightness: " + String(brightness);
   
   const char* temp_chars = temp.c_str();
   
   long now = millis();
    if (now - lastMsg > 5000) {
      lastMsg = now;
      ++value;
    snprintf (msg, 200, temp_chars, value);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish(topic, msg);
    }
    
   delay(5000);
   }
   
  
