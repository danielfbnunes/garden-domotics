## Domotics - P3G3

Esta página serve de suporte para documentação do projeto final da disciplina de Introdução à Engenharia de Software.

A domótica tem como objetivo melhorar o controlo e a facilidade das rotinas e tarefas residenciais, como a iluminação, climatização do ambiente, economia de recursos e controle de segurança. 

O nosso objetivo com este trabalho é aplicar esta tecnologia numa estufa pessoal de modo a que qualquer pessoa possa ter a sua pequena horta ou jardim em sua casa, reduzindo as suas despesas e a oportunidade de controlar e consultar o estado das suas plantações onde quer que estejam.

### Membros

André Brandão 84916 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Daniel Nunes 84793

Pedro Ferreira &nbsp;84735 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rafael Direito 84921

Rafael Teixeira &nbsp;84746.