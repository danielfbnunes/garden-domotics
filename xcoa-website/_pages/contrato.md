---
layout: page
title:  ""
---

# Contrato

A domótica tem como objetivo melhorar o controlo e a facilidade das rotinas e tarefas residenciais, como a iluminação, climatização do ambiente, economia de recursos e controle de segurança. 

O nosso objetivo com este trabalho é aplicar esta tecnologia numa estufa pessoal de modo a que qualquer pessoa possa ter a sua pequena horta ou jardim em sua casa, reduzindo as suas despesas e a oportunidade de controlar e consultar o estado das suas plantações onde quer que estejam.

### Funcionalidades

##### Para ir ao encontro dos objetivos do nosso trabalho, este irá apresentar as seguintes funcionalidades:

* Consultar o estado da estufa e de todas as suas divisões através de um painel de controlo intuitivo. Entende-se com estado, toda a informação recolhida pelos sensores.
* Controlo de luzes automático e manual.
* Controlo da temperatura.
* Manter um histórico de todos os eventos e configurações ao longo do tempo.
* Reportar ao utilizador quaisquer incidentes fora do normal. Entende-se por fora do normal qualquer estado definido previamente pelo utilizador.
* Apresentar gráficos e tabelas bem como médias e outros dados de interesse através do painel de controlo.
* Permitir criar perfis. Onde se define as configurações para cada tipo de planta.
* Cada vaso terá uma planta, um conjunto de sensores e uma lâmpada, de modo a que seja possível no painel de controlo consultar o estado de cada vaso, mas também permite agrupar os vasos por tipo de planta.
* Controlo de abertura da porta através de RFID. (Útil para afastar as crianças/animais da estufa sem ter de andar com chaves ou cartões, pois é possível emular RFID com o próprio telemóvel.)
* Receber notificações de quando a porta controlada por RFID fôr abertas.


### Logística

* Photoresistors 
* DHT11 temperature sensors
* PIR movement sensor
* Sensor para detetar os níveis de CO2 e O2
* Sensor de humidade.
* Arduinos
* RaspberryPi
* Smartphone