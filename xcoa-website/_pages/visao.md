---
layout: page
title:  ""
---

# Visão Geral

A visão define o âmbito do produto com uma perspetiva geral. Captura a essência da solução prevista sob a forma de requisitos de alto nível e restrições do projeto, assegurando
que as metas do projeto são definidas em termos do que o cliente precisa, isto dará ao leitor uma visão geral do sistema a ser desenvolvido a partir de uma perspectiva de requisitos comportamentais.

Ele fornece a entrada para o processo de aprovação de projeto e é, por conseguinte, estritamente relacionado com o modelo de negócios. 
Ele responde às questões, _por quê?_, _o quê?_ e _para quem?_ do projeto, e é um medidor contra o qual todas as decisões futuras deverão ser validadas.

A visão permite ainda que a equipa discuta possíveis vantagens e desvantagens do projeto, o
cronograma e os recursos necessários para definir um plano viável para a construção do projeto que
cumpra os requisitos do cliente.

### Motivação

Aos poucos, o mundo está a mudar e são cada vez as mais as pessoas que se preocupam com aquilo que comem. 
Por isso, não é de estranhar que ter uma horta em casa comece a ser cada vez mais frequente. 
Como tal, é necessário arranjar métodos simples, eficazes e inovadores para cultivar os nossos próprios legumes e frutas.

### Definição do Produto

Para simplificar e aumentar a eficácia dos jardins pessoais o nosso projeto, baseado na tecnologia da domótica, irá permitir ao seus utilizadores
mantêr, controlar e automatizar a sua horta de um modo simples e moderno que os permite obter os legumes e frutas da melhor qualidade possível.


### Funcionalidades:

* Consultar o estado da estufa e de todas as suas divisões através de um painel de controlo intuitivo. Entende-se com estado, toda a informação recolhida pelos sensores.
* Controlo de luzes automático e manual.
* Controlo da temperatura.
* Manter um histórico de todos os eventos e configurações ao longo do tempo.
* Reportar ao utilizador quaisquer incidentes fora do normal. Entende-se por fora do normal qualquer estado definido previamente pelo utilizador.
* Apresentar gráficos e tabelas bem como médias e outros dados de interesse através do painel de controlo.
* Permitir criar perfis. Onde se define as configurações para cada tipo de planta.
* Cada vaso terá uma planta, um conjunto de sensores e uma lâmpada, de modo a que seja possível no painel de controlo consultar o estado de cada vaso, mas também permite agrupar os vasos por tipo de planta.
* Controlo de abertura da porta através de RFID. (Útil para afastar as crianças/animais da estufa sem ter de andar com chaves ou cartões, pois é possível emular RFID com o próprio telemóvel.)
* Receber notificações de quando a porta controlada por RFID fôr abertas.

### Casos de Utilização

{: .use_case_img}
![Casos de Utilização](../assets/img/casos_de_uso.jpg)

{: .table .table-striped .table-bordered}
| Casos de Utilização | Descrição |
|---------------------|-----------|
| Consultar o estado de uma planta | O utilizador usa o dashboard para verificar os valores dos sensores de uma determinada planta. |
| Consultar o histórico de eventos e configurações | O utilizador usa a opção de ver o histórica para consultar todos os eventos e configurações passadas. |
| Definir configurações para uma determinada planta | O utilizador depois de adicionar uma nova planta define as configurações que pretender para automatizar o processo. Nomeadamente os _thresholds_ para ativar os atuadores. |
| Criar um perfil para um tipo de planta | O utilizador cria um novo perfil para uma determinada planta. Podendo depois definir configurações para essa planta. |
| Ligar/Desligar a Luz | O utilizador usa o dashboard para ligar a luz de uma lâmpada manualmente associada a uma determinada planta. |
| Ligar/Desligar a Ventoinha | O utilizador usa o dashboard para ligar uma ventoinha manualmente associada a uma determinada planta. |
|---------------------|-----------|